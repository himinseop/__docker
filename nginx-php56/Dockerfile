FROM centos:6.6
MAINTAINER Minseop Ahn <himinseop@gmail.com>
WORKDIR /usr/local/src

RUN yum -y update
RUN yum -y install epel-release
RUN yum -y install tar gcc \
        libxml2-devel openssl-devel curl-devel libpng-devel mysql-devel \
        php-mcrypt libmcrypt-devel \
        nginx \
        ntp supervisor \
        git autoconf \
    && yum clean all

# Install PHP
ENV PHP_VERSION 5.6.27
ADD http://kr1.php.net/distributions/php-$PHP_VERSION.tar.gz .
RUN tar xvfz php-*.tar.gz \
    && cd php-$PHP_VERSION \
    && ./configure --disable-debug --enable-xml --with-libxml-dir=/usr/local/libxml --enable-mbstring --with-zlib --with-curl --with-iconv --enable-sockets --with-mcrypt --enable-fd-setsize=8192 --enable-fpm --with-config-file-path=/etc/ --with-mysql --with-libdir=lib64 --with-mysqli=/usr/bin/mysql_config --with-openssl --with-gd --enable-zip --with-pdo-mysql --with-pdo-sqlite \
    && make && make install && make clean

# Install PhalconPHP Framework
RUN git clone git://github.com/phalcon/cphalcon.git
RUN cd cphalcon/build \
    && ./install

# Xdebug
RUN pecl install xdebug

# Set env
COPY source/php/php.ini /etc/php.ini
COPY source/php/php-fpm.conf /usr/local/etc/php-fpm.conf
COPY source/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf
COPY source/supervisor/supervisord.conf /etc/supervisord.conf
COPY source/system/sysctl.conf /etc/sysctl.conf
COPY source/boot /usr/local/bin

RUN chmod 644 /etc/supervisord.conf
RUN chmod 755 /usr/local/bin/boot

WORKDIR /home
EXPOSE 80
VOLUME ["/home/httpd"]
CMD ["boot"]
